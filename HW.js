// 1. Напишите код, который будет присваивать переменной "result" значение суммы переменных "x" и "y" - в случае если x < y, разность "x" и "y" - в случае если x > y, и их произведение в остальных случаях.
function mathResult(x, y) {
    let result;

    if (x < y) {
        result = x + y;
    } else if (x > y) {
        result = x - y;
    } else {
        result = x * y;
    }

    return result;
};

//let result = mathResult(4, 6);
//let result = mathResult(12, 6);
//let result = mathResult(5, 5);
//console.log(result)

/********************************************************************************************************************************************************************/


// 2. Есть два массива случайной длины заполненные случайными числами. Вам нужно написать функцию, которая считает сумму всех элементов обоих массивов.
let a = [1, 2, 3, -5, 0, 10];
let b = [5, -4, 17, 9];

/*1*/
function arraysSum(arrayA, arrayB) {
    let sumA = 0;
    let sumB = 0;
    let sumAB;

    for (let i in arrayA) {
        sumA = sumA + arrayA[i];
    }

    for (let i in arrayB) {
        sumB = sumB + arrayB[i];
    }

    sumAB = sumA + sumB;

    return sumAB;

};

let arraysSumResult = arraysSum(a, b);

//console.log(arraysSumResult);

/*2*/
function arraysSum2(arrayA, arrayB) {
    let arrayAB = arrayA.concat(arrayB);
    let sumArrayAB = 0;

    for (let i in arrayAB) {
        sumArrayAB = sumArrayAB + arrayAB[i];
    }

    return sumArrayAB;

};

let arraysSumResult2 = arraysSum2(a, b);

//console.log(arraysSumResult2);

/********************************************************************************************************************************************************************/


// 3. Дан массив, в котором содержатся элементы true и false. Написать функцию, которая подсчитает количество элементов true в массиве.

let someArray = [true, false, false, true, false, false, true, true, false, false, ];

function trueCounter(array) {
    let trueSum = 0;
    for (let i in array) {
        if (array[i] == 1) {
            trueSum = trueSum + 1;
        }
    }

    return trueSum;
};

let trueSumResult = trueCounter(someArray);

//console.log(trueSumResult);

/*******************************************************************************************************************************************************************/


// 4. Напишите функцию, которая вычисляет двойной факториал числа.
function doubleFactorialCalculation(num) {
    let arrayNum = [];
    let factorial = 1;

    while (num >= 1) {
        arrayNum.push(num);
        num = num - 2;
    }

    for (let i in arrayNum) {
        factorial = factorial * arrayNum[i];
    }

    return factorial;

}

let factorialResult = doubleFactorialCalculation(28);

//console.log(factorialResult);


/******************************************************************************************************************************************************************/

//5. Дано два объекта person, в них содержатся поля name и age.
// 5.1 Написать функцию compareAge, которая будет сравнивать два объекта по возрасту и возвращать шаблонную строку как результат. Шаблонная строка выглядит следующим образом: <Person 1 name> is <older | younger | the same age as> <Person 2 name>

const person1 = {
    name: "Melissa", 
    age: 20
};

const person2 = {
    name: "Dave", 
    age: 45
}

let ageResult;

function compareAge(obj1, obj2) {
    if(obj1.age > obj2.age){
        return ageResult = `${obj1.name} is older as ${obj2.name}`;
    } else if (obj1.age < obj2.age) {
        return ageResult = `${obj1.name} is younger as ${obj2.name}`;
    } else {
        return ageResult = `${obj1.name} is the same age as ${obj2.name}`;
    }
};

let compareAgeResult = compareAge(person1, person2);

//console.log(compareAgeResult);

/****************************************************************************************************************************************************************/

// 5.2 Создать массив из нескольких объектов person (3-5 элементов) и отсортировать его по возрасту: а) по возрастанию б) по спаданию

const person = [
    {name: "Melissa", age: 15},
    {name: "Dave", age: 70},
    {name: "Linda", age: 20},
    {name: "Mila", age: 48},
    {name: "Utah", age: 37},
    {name: "Jin", age: 24}
];


function mySortAscending(a, b){
    if(a.age > b.age)
        return 1
    if(a.age < b.age)
        return -1
    return 0
};

function mySortDecline(a, b){
    if(a.age < b.age)
        return 1
    if(a.age > b.age)
        return -1
    return 0
};


//person.sort(mySortAscending);
//person.sort(mySortDecline);
//console.log(person);

